├─ abbrev@1.0.7
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/abbrev-js
│  └─ licenseFile: node_modules/abbrev/LICENSE
├─ acorn-globals@2.0.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/ForbesLindesay/acorn-globals
│  └─ licenseFile: node_modules/acorn-globals/LICENSE
├─ acorn@2.7.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/ternjs/acorn
│  └─ licenseFile: node_modules/acorn-globals/node_modules/acorn/LICENSE
├─ acorn@3.0.4
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/ternjs/acorn
│  └─ licenseFile: node_modules/acorn/LICENSE
├─ align-text@0.1.4
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jonschlinkert/align-text
│  └─ licenseFile: node_modules/align-text/LICENSE
├─ amd-conversion-analyser@1.0.0
│  ├─ licenses: Apache-2.0
│  └─ licenseFile: LICENSE.txt
├─ amdefine@1.0.0
│  ├─ licenses: BSD-3-Clause AND MIT
│  ├─ repository: https://github.com/jrburke/amdefine
│  └─ licenseFile: node_modules/amdefine/LICENSE
├─ argparse@1.0.7
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/nodeca/argparse
│  └─ licenseFile: node_modules/argparse/LICENSE
├─ array-union@1.0.1
│  ├─ licenses: MIT
│  └─ repository: https://github.com/sindresorhus/array-union
├─ array-uniq@1.0.2
│  ├─ licenses: MIT
│  └─ repository: https://github.com/sindresorhus/array-uniq
├─ arrify@1.0.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/arrify
│  └─ licenseFile: node_modules/arrify/license
├─ assertion-error@1.0.1
│  ├─ licenses: MIT
│  └─ repository: https://github.com/chaijs/assertion-error
├─ async@0.2.10
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/caolan/async
│  └─ licenseFile: node_modules/uglify-js/node_modules/async/LICENSE
├─ async@1.0.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/caolan/async
│  └─ licenseFile: node_modules/async/LICENSE
├─ async@1.5.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/caolan/async
│  └─ licenseFile: node_modules/handlebars/node_modules/async/LICENSE
├─ balanced-match@0.3.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/juliangruber/balanced-match
│  └─ licenseFile: node_modules/balanced-match/LICENSE.md
├─ brace-expansion@1.1.3
│  ├─ licenses: MIT
│  └─ repository: https://github.com/juliangruber/brace-expansion
├─ camelcase@1.2.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/camelcase
│  └─ licenseFile: node_modules/camelcase/license
├─ center-align@0.1.3
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jonschlinkert/center-align
│  └─ licenseFile: node_modules/center-align/LICENSE
├─ chai-things@0.2.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/RubenVerborgh/Chai-Things
│  └─ licenseFile: node_modules/chai-things/LICENSE.md
├─ chai@3.5.0
│  ├─ licenses: MIT
│  └─ repository: https://github.com/chaijs/chai
├─ cliui@2.1.0
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/bcoe/cliui
│  └─ licenseFile: node_modules/cliui/LICENSE.txt
├─ colors@1.0.3
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/Marak/colors.js
│  └─ licenseFile: node_modules/winston/node_modules/colors/MIT-LICENSE.txt
├─ colors@1.1.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/Marak/colors.js
│  └─ licenseFile: node_modules/colors/LICENSE
├─ commander@0.6.1
│  ├─ licenses: MIT*
│  └─ repository: https://github.com/visionmedia/commander.js
├─ commander@2.3.0
│  ├─ licenses: MIT*
│  └─ repository: https://github.com/visionmedia/commander.js
├─ concat-map@0.0.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/substack/node-concat-map
│  └─ licenseFile: node_modules/concat-map/LICENSE
├─ cycle@1.0.3
│  ├─ licenses: Public domain
│  └─ repository: https://github.com/dscape/cycle
├─ debug@2.2.0
│  ├─ licenses: MIT
│  └─ repository: https://github.com/visionmedia/debug
├─ decamelize@1.2.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/decamelize
│  └─ licenseFile: node_modules/decamelize/license
├─ deep-eql@0.1.3
│  ├─ licenses: MIT
│  └─ repository: https://github.com/chaijs/deep-eql
├─ deep-is@0.1.3
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/thlorenz/deep-is
│  └─ licenseFile: node_modules/deep-is/LICENSE
├─ diff@1.4.0
│  ├─ licenses: BSD
│  └─ repository: https://github.com/kpdecker/jsdiff
├─ escape-regex-string@1.0.4
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/monotonee/escape-regex-string
│  └─ licenseFile: node_modules/escape-regex-string/LICENSE
├─ escape-string-regexp@1.0.2
│  ├─ licenses: MIT
│  └─ repository: https://github.com/sindresorhus/escape-string-regexp
├─ escodegen@1.8.0
│  ├─ licenses: BSD-2-Clause
│  ├─ repository: https://github.com/estools/escodegen
│  └─ licenseFile: node_modules/escodegen/LICENSE.source-map
├─ esprima@2.7.2
│  ├─ licenses: BSD-2-Clause
│  ├─ repository: https://github.com/jquery/esprima
│  └─ licenseFile: node_modules/esprima/LICENSE.BSD
├─ estraverse@1.9.3
│  ├─ licenses: BSD
│  ├─ repository: https://github.com/estools/estraverse
│  └─ licenseFile: node_modules/estraverse/LICENSE.BSD
├─ esutils@2.0.2
│  ├─ licenses: BSD
│  ├─ repository: https://github.com/estools/esutils
│  └─ licenseFile: node_modules/esutils/LICENSE.BSD
├─ eyes@0.1.8
│  ├─ licenses: MIT
│  └─ licenseFile: node_modules/eyes/LICENSE
├─ fast-levenshtein@1.1.3
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/hiddentao/fast-levenshtein
│  └─ licenseFile: node_modules/fast-levenshtein/LICENSE.md
├─ fileset@0.2.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/mklabs/node-fileset
│  └─ licenseFile: node_modules/fileset/LICENSE-MIT
├─ formatio@1.1.1
│  ├─ licenses: BSD*
│  ├─ repository: https://github.com/busterjs/formatio
│  └─ licenseFile: node_modules/formatio/LICENSE
├─ glob@3.2.3
│  ├─ licenses: BSD
│  ├─ repository: https://github.com/isaacs/node-glob
│  └─ licenseFile: node_modules/mocha/node_modules/glob/LICENSE
├─ glob@5.0.15
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/node-glob
│  └─ licenseFile: node_modules/fileset/node_modules/glob/LICENSE
├─ glob@6.0.4
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/node-glob
│  └─ licenseFile: node_modules/glob/LICENSE
├─ globals@9.4.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/globals
│  └─ licenseFile: node_modules/globals/license
├─ globby@4.0.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/globby
│  └─ licenseFile: node_modules/globby/license
├─ graceful-fs@2.0.3
│  ├─ licenses: BSD
│  ├─ repository: https://github.com/isaacs/node-graceful-fs
│  └─ licenseFile: node_modules/graceful-fs/LICENSE
├─ growl@1.8.1
│  ├─ licenses: MIT*
│  └─ repository: https://github.com/visionmedia/node-growl
├─ handlebars@4.0.5
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/wycats/handlebars.js
│  └─ licenseFile: node_modules/handlebars/LICENSE
├─ has-flag@1.0.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/has-flag
│  └─ licenseFile: node_modules/has-flag/license
├─ inflight@1.0.4
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/inflight
│  └─ licenseFile: node_modules/inflight/LICENSE
├─ inherits@2.0.1
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/inherits
│  └─ licenseFile: node_modules/inherits/LICENSE
├─ is-absolute@0.1.7
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jonschlinkert/is-absolute
│  └─ licenseFile: node_modules/is-absolute/LICENSE
├─ is-buffer@1.1.3
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/feross/is-buffer
│  └─ licenseFile: node_modules/is-buffer/LICENSE
├─ is-relative@0.1.3
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jonschlinkert/is-relative
│  └─ licenseFile: node_modules/is-relative/LICENSE-MIT
├─ isexe@1.1.2
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/isexe
│  └─ licenseFile: node_modules/isexe/LICENSE
├─ isstream@0.1.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/rvagg/isstream
│  └─ licenseFile: node_modules/isstream/LICENSE.md
├─ istanbul@0.4.3
│  ├─ licenses: BSD-3-Clause
│  ├─ repository: https://github.com/gotwarlost/istanbul
│  └─ licenseFile: node_modules/istanbul/LICENSE
├─ jade@0.26.3
│  ├─ licenses: MIT*
│  ├─ repository: https://github.com/visionmedia/jade
│  └─ licenseFile: node_modules/jade/LICENSE
├─ js-yaml@3.5.5
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/nodeca/js-yaml
│  └─ licenseFile: node_modules/js-yaml/LICENSE
├─ kind-of@3.0.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jonschlinkert/kind-of
│  └─ licenseFile: node_modules/kind-of/LICENSE
├─ lazy-cache@1.0.3
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jonschlinkert/lazy-cache
│  └─ licenseFile: node_modules/lazy-cache/LICENSE
├─ levn@0.3.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/gkz/levn
│  └─ licenseFile: node_modules/levn/LICENSE
├─ lolex@1.3.2
│  ├─ licenses: BSD-3-Clause
│  ├─ repository: https://github.com/sinonjs/lolex
│  └─ licenseFile: node_modules/lolex/LICENSE
├─ longest@1.0.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jonschlinkert/longest
│  └─ licenseFile: node_modules/longest/LICENSE
├─ lru-cache@2.7.3
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/node-lru-cache
│  └─ licenseFile: node_modules/lru-cache/LICENSE
├─ minimatch@0.2.14
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/isaacs/minimatch
│  └─ licenseFile: node_modules/mocha/node_modules/minimatch/LICENSE
├─ minimatch@2.0.10
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/minimatch
│  └─ licenseFile: node_modules/fileset/node_modules/minimatch/LICENSE
├─ minimatch@3.0.0
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/minimatch
│  └─ licenseFile: node_modules/minimatch/LICENSE
├─ minimist@0.0.10
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/substack/minimist
│  └─ licenseFile: node_modules/optimist/node_modules/minimist/LICENSE
├─ minimist@0.0.8
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/substack/minimist
│  └─ licenseFile: node_modules/mkdirp/node_modules/minimist/LICENSE
├─ minimist@1.2.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/substack/minimist
│  └─ licenseFile: node_modules/minimist/LICENSE
├─ mkdirp@0.3.0
│  ├─ licenses: MIT/X11
│  ├─ repository: https://github.com/substack/node-mkdirp
│  └─ licenseFile: node_modules/jade/node_modules/mkdirp/LICENSE
├─ mkdirp@0.5.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/substack/node-mkdirp
│  └─ licenseFile: node_modules/mkdirp/LICENSE
├─ mocha@2.4.5
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/mochajs/mocha
│  └─ licenseFile: node_modules/mocha/LICENSE
├─ ms@0.7.1
│  ├─ licenses: MIT*
│  ├─ repository: https://github.com/guille/ms.js
│  └─ licenseFile: node_modules/ms/LICENSE
├─ nopt@3.0.6
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/npm/nopt
│  └─ licenseFile: node_modules/nopt/LICENSE
├─ object-assign@4.0.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/object-assign
│  └─ licenseFile: node_modules/object-assign/license
├─ once@1.3.3
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/once
│  └─ licenseFile: node_modules/once/LICENSE
├─ optimist@0.6.1
│  ├─ licenses: MIT/X11
│  ├─ repository: https://github.com/substack/node-optimist
│  └─ licenseFile: node_modules/optimist/LICENSE
├─ optionator@0.8.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/gkz/optionator
│  └─ licenseFile: node_modules/optionator/LICENSE
├─ path-is-absolute@1.0.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/path-is-absolute
│  └─ licenseFile: node_modules/path-is-absolute/license
├─ pify@2.3.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/sindresorhus/pify
│  └─ licenseFile: node_modules/pify/license
├─ pinkie-promise@2.0.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/floatdrop/pinkie-promise
│  └─ licenseFile: node_modules/pinkie-promise/license
├─ pinkie@2.0.4
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/floatdrop/pinkie
│  └─ licenseFile: node_modules/pinkie/license
├─ pkginfo@0.3.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/indexzero/node-pkginfo
│  └─ licenseFile: node_modules/pkginfo/LICENSE
├─ prelude-ls@1.1.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/gkz/prelude-ls
│  └─ licenseFile: node_modules/prelude-ls/LICENSE
├─ repeat-string@1.5.4
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jonschlinkert/repeat-string
│  └─ licenseFile: node_modules/repeat-string/LICENSE
├─ resolve@1.1.7
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/substack/node-resolve
│  └─ licenseFile: node_modules/resolve/LICENSE
├─ rewire@2.5.1
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jhnns/rewire
│  └─ licenseFile: node_modules/rewire/LICENSE
├─ right-align@0.1.3
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jonschlinkert/right-align
│  └─ licenseFile: node_modules/right-align/LICENSE
├─ samsam@1.1.2
│  ├─ licenses: BSD*
│  ├─ repository: https://github.com/busterjs/samsam
│  └─ licenseFile: node_modules/samsam/LICENSE
├─ sigmund@1.0.1
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/sigmund
│  └─ licenseFile: node_modules/sigmund/LICENSE
├─ sinon@1.17.3
│  ├─ licenses: BSD-3-Clause
│  ├─ repository: https://github.com/cjohansen/Sinon.JS
│  └─ licenseFile: node_modules/sinon/LICENSE
├─ source-map@0.2.0
│  ├─ licenses: BSD
│  ├─ repository: https://github.com/mozilla/source-map
│  └─ licenseFile: node_modules/source-map/LICENSE
├─ source-map@0.4.4
│  ├─ licenses: BSD-3-Clause
│  └─ repository: https://github.com/mozilla/source-map
├─ source-map@0.5.3
│  ├─ licenses: BSD-3-Clause
│  └─ repository: https://github.com/mozilla/source-map
├─ sprintf-js@1.0.3
│  ├─ licenses: BSD-3-Clause
│  ├─ repository: https://github.com/alexei/sprintf.js
│  └─ licenseFile: node_modules/sprintf-js/LICENSE
├─ stack-trace@0.0.9
│  ├─ licenses: MIT*
│  ├─ repository: https://github.com/felixge/node-stack-trace
│  └─ licenseFile: node_modules/stack-trace/License
├─ supports-color@1.2.0
│  ├─ licenses: MIT
│  └─ repository: https://github.com/sindresorhus/supports-color
├─ supports-color@3.1.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/chalk/supports-color
│  └─ licenseFile: node_modules/supports-color/license
├─ type-check@0.3.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/gkz/type-check
│  └─ licenseFile: node_modules/type-check/LICENSE
├─ type-detect@0.1.1
│  ├─ licenses: MIT
│  └─ repository: https://github.com/chaijs/type-detect
├─ type-detect@1.0.0
│  ├─ licenses: MIT
│  └─ repository: https://github.com/chaijs/type-detect
├─ uglify-js@2.6.2
│  ├─ licenses: BSD-2-Clause
│  ├─ repository: https://github.com/mishoo/UglifyJS2
│  └─ licenseFile: node_modules/uglify-js/LICENSE
├─ uglify-to-browserify@1.0.2
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/ForbesLindesay/uglify-to-browserify
│  └─ licenseFile: node_modules/uglify-to-browserify/LICENSE
├─ util@0.10.3
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/defunctzombie/node-util
│  └─ licenseFile: node_modules/util/LICENSE
├─ which@1.2.4
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/isaacs/node-which
│  └─ licenseFile: node_modules/which/LICENSE
├─ window-size@0.1.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/jonschlinkert/window-size
│  └─ licenseFile: node_modules/window-size/LICENSE-MIT
├─ winston@2.2.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/winstonjs/winston
│  └─ licenseFile: node_modules/winston/LICENSE
├─ wordwrap@0.0.2
│  ├─ licenses: MIT/X11
│  └─ repository: https://github.com/substack/node-wordwrap
├─ wordwrap@0.0.3
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/substack/node-wordwrap
│  └─ licenseFile: node_modules/optimist/node_modules/wordwrap/LICENSE
├─ wordwrap@1.0.0
│  ├─ licenses: MIT
│  ├─ repository: https://github.com/substack/node-wordwrap
│  └─ licenseFile: node_modules/wordwrap/LICENSE
├─ wrappy@1.0.1
│  ├─ licenses: ISC
│  ├─ repository: https://github.com/npm/wrappy
│  └─ licenseFile: node_modules/wrappy/LICENSE
└─ yargs@3.10.0
   ├─ licenses: MIT
   ├─ repository: https://github.com/bcoe/yargs
   └─ licenseFile: node_modules/yargs/LICENSE

