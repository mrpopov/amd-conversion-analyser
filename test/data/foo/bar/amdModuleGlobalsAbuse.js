define("test/data/amdModuleGlobalsAbuse", [], function() {
    Foo.bar();

    Excluded.variableRightHere();

    return {
        "callback": Bar.baz
    };
});
