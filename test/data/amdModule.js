define("test/data/amdModule", ["Foobar"], function(Foobar) {
    var foo = true;
    var bar = false;

    Excluded.variableRightHere();

    return Foobar(foo, bar);
});
