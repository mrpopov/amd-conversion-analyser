'use strict';
var chai = require("chai");
chai.should();
chai.use(require('chai-things'));

var expect = chai.expect;

describe("DetectAMD", function() {
    var detectAMD = require("../src/lib/detect-amd");

    it("accepts source and returns an array", function() {
        var result = detectAMD("foo = 1;");
        expect(result).to.be.instanceof(Array);
    });

    describe("on define() blocks", function() {

        it("detects named modules", function() {
            var result1 = detectAMD("define('foo', ['bar','baz'], function() {});");
            var result2 = detectAMD("define('foo', function() {});");
            var result3 = detectAMD("define('foo', {});");

            [result1, result2, result3].forEach(function(r) {
                expect(r).to.be.instanceof(Array);
                expect(r).to.have.lengthOf(1);
                expect(r[0]).to.have.property('name', 'foo');
                expect(r[0]).to.have.property('type', 'define');
            });
        });

        it("ignores non-AMD calls", function() {
            var result1 = detectAMD("bar();");
            var result2 = detectAMD("definer('foo', {});");
            var result3 = detectAMD("define.it('foo', {});");
            var result4 = detectAMD("var foo = 1;");

            [result1, result2, result3, result4].forEach(function(r) {
                expect(r).to.be.instanceof(Array);
                expect(r).to.have.lengthOf(0);
            });
        });

        it("returns modules in source order", function() {
            var result = detectAMD("define('foo', {}); define('bar', {}); define('baz', {});");
            expect(result).to.have.lengthOf(3);
            expect(result[0]).to.have.property('name','foo');
            expect(result[1]).to.have.property('name','bar');
            expect(result[2]).to.have.property('name','baz');
        });

        it("detects anonymous modules", function() {
            var result = detectAMD([
                "var foo = 1;",
                "define(['bar','baz'], function() {});",
                "bar(); define.it('something', {});",
                "define(function() {});",
                "define({});",
            ].join('\n'));

            expect(result).to.have.lengthOf(3);
        });

    });

    describe("on require() blocks", function() {

        it("detects asynchronous calls", function() {
            var result = detectAMD("require(['bar','baz'], function() {});");
            expect(result).to.have.lengthOf(1);
            expect(result[0]).to.have.property('type','require');
        });

    });

    describe("on mixed source", function() {

        it("returns detected modules in source order", function() {
            var result = detectAMD([
                "var foo = 1;",
                "define('foo', ['bar','baz'], function() {});",
                "bar();",
                "require(['foo']);",
                "define.it('something', {});",
                "define('bar', function() {});",
                "define('baz', {});",
                "require(['bar','baz'], function() {});"
            ].join('\n'));

            expect(result).to.have.lengthOf(5);
            expect(result).to.have.nested.property('[0].type', 'define');
            expect(result).to.have.nested.property('[1].type', 'require');
            expect(result).to.have.nested.property('[2].type', 'define');
            expect(result).to.have.nested.property('[3].type', 'define');
            expect(result).to.have.nested.property('[4].type', 'require');
        });

    });

});