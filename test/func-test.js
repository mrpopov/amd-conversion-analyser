'use strict';
let chai = require("chai");
chai.should();
chai.use(require('chai-things'));
let expect = chai.expect;
let assert = chai.assert;
let sinon = require('sinon');

let rewire = require("rewire");
let ConversionDetector = rewire("../src/lib/conversiondetector");

let searchDir = "./test/data";
let baseConfig = {
    exclude: {
        variables: [
            'Excluded.variableRightHere'
        ],
        patterns: [
            'excludedFile.js'
        ]
    },
    symbols: {
        OK: "o",
        ERR: "x",
        ALERT: "!"
    },
    env: {
        amd: true
    }
};

let loggers = [];
let stubs = {
    info: sinon.stub(),
    warn: sinon.stub(),
    error: sinon.stub()
};

function restoreStubs() {
    Object.keys(stubs).forEach(name => stubs[name].reset());
}

let winstonStub = {
    transports: sinon.stub(),
    Logger: function (params) {
        this.info = stubs.info;
        this.warn = stubs.warn;
        this.error = stubs.error;
        this.add = sinon.stub();

        this.level = params && params.level;

        loggers.push(this);
    }
};

sinon.createStubInstance(winstonStub.Logger);

ConversionDetector.__set__({
    'winston': winstonStub
});



describe("func test", function () {
    const detector = new ConversionDetector(searchDir, baseConfig, {
        globals: true,
        utf8: false
    });

    before(function (done) {
        var originalPrintSummary = detector.printSummary.bind(detector);

        sinon.stub(detector, "printSummary").callsFake(function () {
            originalPrintSummary();
            done();
        });

        detector.run();
    });

    after(function () {
        detector.printSummary.restore();
        restoreStubs();
    });

    it("should log the Named AMD module as OK to console", function () {
        expect(stubs.info.withArgs(sinon.match("amdModule.js")).args[0][0])
            .to.have.string(baseConfig.symbols.OK);
    });

    it("should log the Unnamed AMD module as OK to console", function () {
        expect(stubs.info.withArgs(sinon.match("amdModuleNoName.js")).args[0][0])
            .to.have.string(baseConfig.symbols.OK);
    });

    it("should log the require() call as OK to console", function () {
        expect(stubs.info.withArgs(sinon.match("main.js")).args[0][0])
            .to.have.string(baseConfig.symbols.OK);
    });

    it("should log the globals abuse as an ALERT to console", function () {
        expect(stubs.warn.withArgs(sinon.match("amdModuleGlobalsAbuse.js")).args[0][0])
            .to.have.string(baseConfig.symbols.ALERT).and
            .to.have.string("Globals detected");
    });

    it("should log the globals and line:column to console", function () {
        expect(stubs.warn.withArgs(sinon.match("Bar.baz")).args[0][0])
            .to.have.string("7:20");
        expect(stubs.warn.withArgs(sinon.match("Foo.bar")).args[0][0])
            .to.have.string("2:4");
    });

    it("should log the non-AMD file as an ALERT to console", function () {
        expect(stubs.error.withArgs(sinon.match("nonAmdModule.js")).args[0][0])
            .to.have.string(baseConfig.symbols.ERR).and
            .to.have.string("Not an AMD module");
    });

    it("should log all directories to console", function () {
        expect(stubs.info.withArgs("./test/data").calledOnce).to.be.ok;
        expect(stubs.info.withArgs("./test/data/foo").calledOnce).to.be.ok;
        expect(stubs.info.withArgs("./test/data/foo/bar").calledOnce).to.be.ok;
        expect(stubs.info.withArgs("./test/data/main").calledOnce).to.be.ok;
    });

    it("should summarize the number of AMD files", function () {
        expect(stubs.info.withArgs(sinon.match("as AMD modules:")).args[0][0])
            .to.have.string("as AMD modules: 4");
        expect(stubs.info.withArgs(sinon.match("define() calls:")).args[0][0])
            .to.have.string("define() calls: 3");
        expect(stubs.info.withArgs(sinon.match("require() calls:")).args[0][0])
            .to.have.string("require() calls: 1");
    });

    it("should summarize the number of non-AMD files", function () {
        expect(stubs.warn.withArgs(sinon.match("not converted:")).args[0][0])
            .to.have.string("not converted: 2");
    });

    it("should summarize the number of globals found", function () {
        expect(stubs.warn.withArgs(sinon.match("globals found:")).args[0][0])
            .to.have.string("globals found: 2");
    });

    it("should summarize the percentage of modules converted", function () {
        expect(stubs.info.withArgs(sinon.match("% converted")).args[0][0])
            .to.have.string("67% converted");
    });
});


describe("func test - argv", function () {
    let fileLogLevels = [undefined, 'debug', 'info', 'warn', 'error'];
    let okFlag = [true, false];

    function createDetector(argv, searchDirParam = searchDir, baseConfigParam = baseConfig) {
        return new ConversionDetector(searchDirParam, baseConfigParam, argv);
    }

    fileLogLevels.forEach(function (logLevel) {
        describe("fun test - argv - file-log-level: " + logLevel, function () {
            loggers = [];

            before(function () {
                createDetector({'file-log-level': logLevel});
            });

            after(function () {
                loggers = [];
                restoreStubs();
            });

            it("should set proper file log level for: " + logLevel, function () {
                expect(loggers.map(logger => logger.level).includes(logLevel)).to.be.ok;
            });
        });
    });

    okFlag.forEach(function (flag) {
        describe(`fun test - argv - no-ok: ${!flag}`, function () {
            before(function (done) {
                let detector = this.detector = createDetector({'ok': flag});
                let originalPrintFileSummary = detector.printFileSummary.bind(detector);

                sinon.stub(detector, "printFileSummary").callsFake(function () {
                    originalPrintFileSummary();
                    done();
                });

                detector.run();
            });

            after(function () {
                this.detector.printFileSummary.restore();
                restoreStubs();
            });

            it(`should ${(flag ? "" : "not ")}output already converted files`, function () {
                assert.strictEqual(stubs.info.calledWithMatch(sinon.match("amdModule.js")), flag);
            });
        });
    });
});

describe("func test - save", function () {
    const saveLocation = 'file.to.save.json';

    function createDetector(save = false) {
        const argv = {
            globals: true,
            utf8: false,
        };

        if (save) {
            argv.save = save;
        }

        const detector = new ConversionDetector(searchDir, baseConfig, argv);
        sinon.stub(detector, "_saveToFile");

        detector.run();

        return detector;
    }


    it("should call _saveToFile with proper arguments if save was passed", function () {
        const detector = createDetector(saveLocation);

        assert.isOk(detector._saveToFile.calledOnce);

        const args = detector._saveToFile.getCall(0).args;

        assert.deepEqual(JSON.parse(JSON.stringify(args[0])), {
            "files": {
                "./test/data": [
                    {
                        "filepath": "./test/data/amdModule.js",
                        "amd": true,
                        "globals": false,
                        "is": "o"
                    },
                    {
                        "filepath": "./test/data/amdModuleNoName.js",
                        "amd": true,
                        "globals": false,
                        "is": "o"
                    }
                ],
                "./test/data/foo/bar": [
                    {
                        "filepath": "./test/data/foo/bar/amdModuleGlobalsAbuse.js",
                        "amd": true,
                        "is": "!",
                        "globals": true,
                        "globalsList": [
                            {
                                "varName": "Bar.baz",
                                "lineInfo": {
                                    "line": 7,
                                    "column": 20
                                },
                                "isExcluded": false,
                                "fullFilePath": "./test/data/foo/bar/amdModuleGlobalsAbuse.js"
                            },
                            {
                                "varName": "Foo.bar",
                                "lineInfo": {
                                    "line": 2,
                                    "column": 4
                                },
                                "isExcluded": false,
                                "fullFilePath": "./test/data/foo/bar/amdModuleGlobalsAbuse.js"
                            }
                        ],
                        "msg": "Globals detected"
                    }
                ],
                "./test/data/foo": [
                    {
                        "filepath": "./test/data/foo/nonAmdModule.js",
                        "amd": false,
                        "is": "x",
                        "msg": "Not an AMD module"
                    }
                ],
                "./test/data/main": [
                    {
                        "filepath": "./test/data/main/amdUsageInIIFE.js",
                        "amd": false,
                        "is": "x",
                        "msg": "Not an AMD module"
                    },
                    {
                        "filepath": "./test/data/main/main.js",
                        "amd": true,
                        "globals": false,
                        "is": "o"
                    }
                ]
            },
            "globals": [
                {
                    "varName": "Bar.baz",
                    "lineInfo": {
                        "line": 7,
                        "column": 20
                    },
                    "isExcluded": false,
                    "fullFilePath": "./test/data/foo/bar/amdModuleGlobalsAbuse.js"
                },
                {
                    "varName": "Foo.bar",
                    "lineInfo": {
                        "line": 2,
                        "column": 4
                    },
                    "isExcluded": false,
                    "fullFilePath": "./test/data/foo/bar/amdModuleGlobalsAbuse.js"
                }
            ],
            "stats": {
                "percentageOfCovered": 66.66666666666666,
                "countTotalModules": 4,
                "countNonAMD": 2,
                "countGlobals": 2,
                "countAMDDefine": 3,
                "countAMDRequire": 1
            }
        });
        assert.strictEqual(args[1], saveLocation);
    });

    it("should call _saveToFile with proper arguments if save was not passed", function () {
        const detector = createDetector();

        assert.isNotOk(detector._saveToFile.called);
    });
});


