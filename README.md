# AMD conversion analyser

This tool helps you analyse the status of your AMD conversion.

[![build-status](https://bitbucket-badges.useast.atlassian.io/badge/atlassian/amd-conversion-analyser.svg)](https://bitbucket.org/atlassian/amd-conversion-analyser/addon/pipelines/home#!/)

## Installation

    npm install -g amd-conversion-analyser

## Run

    amd /your/source/dir <options>
    
## Options

    --globals           // Show information about used globals in code
    --globals-summary   // Show total information about used globals in code
    --no-utf8           // Don't do pretty print
    --no-summary        // Don't show the conversion summary
    --no-files          // Don't list the files
    --no-ok             // Don't show already converted files
    --save <file>       // Save results to file
    --config <file>     // Use this config file (absolute path only)
    --idea              // Show a link to open the file directly in IntelliJ IDEA
    --idea-globals      // Show a link to open the file directly in IntelliJ IDEA at the position of the found global
    --file-log-level    // Set log level for amd conversion status (debug, info, warn, error); default: info

## Example output

    ./my-plugin/src/main/resources/content/js
        ⚠ Application.js: Globals detected
                49:20 jQuery 
                50:17 _.extend
                230:87 _.extend
        ✗ Trigger.js: Not an AMD module 
    ./my-plugin/src/main/resources/content/js/base
        ✓ DarkFeatures.js
        ✓ IssueLoaderService.js
        ✗ init.js: Not an AMD module 
        ✓ LinksCapturer.js
        ✓ MetadataService.js        
        ⚠ ModelUtils.js: Globals detected
                137:16 $ 
                140:16 _.extend
                143:16 _.clone
    =====================================
    Files identified as AMD modules: 6
    Files not converted: 2
    Illegal globals found: 6
    75% converted
    
## Configuration file

The configuration file is a **JS** file that helps you exclude files, folders and variables and set various other configuration options.
 
If not set using `--config <file>`, the default `config.js` is used. 

### Example configuration

This is the actual content from the default `config.js` file.

    module.exports = {
        "patterns": ["**/*.js"],
        "exclude": {
            "patterns": [
                "**/**-min.js",
                "**/**.min.js",
                "**/target/**",
                "**/dist/**"
            ],
            "variables": []
        },
        "variableDisplaySize": 128,
        "env": {
            "browser": true,
            "amd": true,
            "qunit": true
        }
    };

### Configuration options

The following properties can be set on the configuration object.

#### patterns
Type: `Array`

Default value: `['**/*.js']`

An array of strings, glob patterns, that is used to determine which files will be analysed.

#### exclude.patterns
Type: `Array`

Default value: `[]`

An array of strings, glob patterns, that is used to determine which files will **not** be analysed.

#### exclude.variables
Type: `Array`

Default value: `[]`

An array of strings that is used to filter global variables that will be excluded.

#### env
Type: `Object`

Default value: `{}`

An object of identifiers from different JavaScript environments. Please see the [globals package](https://www.npmjs.com/package/globals) for a full list.

#### variableDisplaySize
Type: `Integer`

Default value: `128`

An integer that is used to set the maximum variable length of a found global.
    
## Development
    git clone git@bitbucket.org:atlassian/amd-conversion-analyser.git
    
    cd amd-conversion-analyser
    
    npm install
    npm run compile

    ./bin/amd.js /your/source/dir <options>
    
## Tips

Open the IDEA link to a file by pressing `CMD` + double click.
    
