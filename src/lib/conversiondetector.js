'use strict';
let acorn = require("acorn");
let glob = require("globby");
let detectAMD = require("./detect-amd");
let detectGlobals = require("acorn-globals");
let globals = require("globals");
let arrayUniq = require("array-uniq");
let _ = require("lodash");
let fs = require("fs");
let path = require("path");
let colors = require('colors/safe');
let winston = require('winston');

colors.setTheme({
    ok: 'grey',
    warn: 'yellow',
    error: 'red'
});

function ConversionDetector(searchDir, config, argv) {
    const symbols = config.symbols;
    const includePatterns = config.patterns || ["**/*.js"];

    const fileMessages = [];

    const globalLogger = createNewLogger();
    const globalsSummaryLogger = createNewLogger();
    const dirLogger = createNewLogger();
    const fileLogger = createNewLogger(argv['file-log-level']);
    const summaryLogger = createNewLogger();

    const isGlobalsSummaryEnabled = !!argv['globals-summary'];
    const shouldOutputGlobals = argv["globals"] || argv["idea-globals"];
    const shouldOutputAlreadyConvertedFiles = argv.ok !== false;

    setupLogTransports();

    this._excludedGlobals = [];
    this._globals = [];
    this._countNonAMD = 0;
    this._countGlobals = 0;
    this._countAMDDefine = 0;
    this._countAMDRequire = 0;

    function createNewLogger(logLevel) {
        return new (winston.Logger)({
            level: logLevel
        });
    }

    function setupLogTransports() {
        if(shouldOutputGlobals) {
            globalLogger.add(winston.transports.Console, {showLevel: false});
        }

        if(isGlobalsSummaryEnabled) {
            globalsSummaryLogger.add(winston.transports.Console, {showLevel: false});
        }

        if(argv.files !== false) {
            dirLogger.add(winston.transports.Console, {showLevel: false});
            fileLogger.add(winston.transports.Console, {showLevel: false});
        }

        if(argv.summary !== false) {
            summaryLogger.add(winston.transports.Console, {showLevel: false});
        }
    }

    this.setupExcludedGlobals = function(envs, existing) {
        let excludes = (typeof existing !== 'undefined' ? existing : []);

        if (envs) {
            Object.keys(envs)
                .filter(name => envs[name])
                .forEach(name => {
                    const environment = globals[name];
                    if (environment) {
                        excludes = arrayUniq(excludes.concat(Object.keys(environment)));
                    }
                });
        }

        return excludes;
    };

    this.getFilename = function(fullFilePath) {
        return path.basename(fullFilePath);
    };

    this.markAsNonAMD = function(fullFilePath) {
        ++this._countNonAMD;
        fileMessages.push({ filepath: fullFilePath, amd: false, is: symbols.ERR, msg: "Not an AMD module" });
    };

    this.markAsAMD = function(fullFilePath, content) {
        ++this._countAMDDefine;
        this.findGlobals(fullFilePath, content);
    };

    this.markAsAMDRequire = function(fullFilePath, content) {
        ++this._countAMDRequire;
        this.findGlobals(fullFilePath, content);
    };

    this.trimToEndOfVariableName = function(node) {
        const namePath = [];
        for (let i = node.parents.length - 1; i >= 0; i--) {
            const parent = node.parents[i];
            if (parent.type === "Identifier") {
                namePath.push(parent.name);
            } else if (parent.type === "MemberExpression") {
                namePath.push(parent.property.name || parent.property.value);
            } else if (parent.type === "ThisExpression") {
                namePath.push("this");
            } else {
                break;
            }
        }
        return namePath.join('.');
    };

    this.isExcludedVariable = function(variable) {
        const accumulated = [];
        const excluded = variable.split(".").some(token => {
            accumulated.push(token);
            const fullToken = accumulated.join(".");
            return config.exclude.variables.indexOf(fullToken) > -1;
        });
        return excluded;
    };

    this.isAMDDefine = function(result) {
        return (typeof result === 'object') && result.hasOwnProperty("type") && result.type === "define";
    };

    this.isAMDRequire = function(result) {
        return (typeof result === 'object') && result.hasOwnProperty("type") && result.type === "require";
    };

    this.isNamedAMDModule = function(result) {
        return this.isAMDDefine(result) && result.hasOwnProperty("name") && Boolean(result.name);
    };

    this.getRelevantGlobals = function(content) {
        return detectGlobals(content).filter(g => {
            if (this._excludedGlobals.indexOf(g.name) > -1) {
                return false;
            }

            const nodes = this.getRelevantOccurrences(content, g.nodes);

            return nodes.length > 0;
        });
    };

    this.getRelevantOccurrences = function(content, nodes) {
        if(!nodes) {
            return [];
        }

        return nodes.filter(node => {
            const varName = this.trimToEndOfVariableName(node);

            if (this.isExcludedVariable(varName)) {
                return false;
            }

            ++this._countGlobals;

            return true;
        });
    };

    this.processRelevantOccurrences = function(content, nodes, fullFilePath) {
        if(!nodes) {
            return [];
        }

        return nodes.map(node => {
            const lineInfo = acorn.getLineInfo(content, node.start);
            const varName = this.trimToEndOfVariableName(node);
            const isExcluded = this.isExcludedVariable(varName);

            return {
                varName,
                lineInfo,
                isExcluded,
                fullFilePath
            };
        });
    };

    this.getIDEALink = function(fullFilePath, line, column) {
        if(line && column) {
            return colors.grey.underline(`http://localhost:63342/api/file/${process.cwd()}/${fullFilePath}:${line}:${column}`);
        }

        return colors.underline(`http://localhost:63342/api/file/${process.cwd()}/${fullFilePath}`);
    };

    this.findGlobals = function(fullFilePath, content) {
        try {
            const globals = this.getRelevantGlobals(content);
            let globalsList = [];

            if (globals.length === 0) {
                fileMessages.push({ filepath: fullFilePath, amd: true, globals: false, is: symbols.OK });
                return;
            }

            if (shouldOutputGlobals) {
                globals.forEach(g => {
                    let gUsages = this.processRelevantOccurrences(content, g.nodes, fullFilePath);
                    globalsList = globalsList.concat(gUsages);
                    this._globals = this._globals.concat(gUsages);
                });
            }

            fileMessages.push({ filepath: fullFilePath, amd: true, is: symbols.ALERT, globals: true, globalsList: globalsList, msg: "Globals detected" });
        } catch (e) {
            fileMessages.push({ filepath: fullFilePath, amd: true, is: symbols.ERR, err: e, msg: "Error finding globals" });
        }
    };

    this.handleFile = function (err, content, fullFilePath, next) {
        try {
            
            if (err) throw new Error(err.message ? err.message : err);
            const result = detectAMD(content);
            const firstNodeInFile = result && result[0];
                     
            if (this.isNamedAMDModule(firstNodeInFile)) {
                this.markAsAMD(fullFilePath, content);
            }
            else if (this.isAMDDefine(firstNodeInFile)) {
                this.markAsAMD(fullFilePath, content);
            }
            else if (this.isAMDRequire(firstNodeInFile)) {                
                this.markAsAMDRequire(fullFilePath, content);
            }
            else {
                this.markAsNonAMD(fullFilePath);
            }
        } catch (e) {
            fileMessages.push({ filepath: fullFilePath, is: symbols.ERR, err: e, msg: "Error parsing file" });
        }

        next();
    };

    this.printFileSummary = function () {
        const consoleFns = {};
        consoleFns[symbols.ERR] = { color: colors.error, logger: fileLogger.error };
        consoleFns[symbols.ALERT] = { color: colors.warn, logger: fileLogger.warn };
        consoleFns[symbols.OK] = { color: colors.ok, logger: fileLogger.info };

        const sortedMessages = _.sortBy(fileMessages, 'filepath');

        let lastPath = "";
        sortedMessages.forEach(message => {
            let {is, filepath, err, msg, globals, globalsList} = message;

            if (!shouldOutputAlreadyConvertedFiles && is === symbols.OK) {
                return;
            }

            let {color, logger} = consoleFns[is];
            let symbol = (is === symbols.OK) ? colors.green(is) : color(is);

            let fileName = this.getFilename(filepath);
            let filePath = path.dirname(filepath);
            let fileStatus = `    ${symbol} ${fileName}`;

            let fileMsg = "";
            if (msg) {
                fileMsg += `: ${msg}`;
            }
            if (err) {
                fileMsg += `: "${err.message}"`;
            }
            if (argv.idea) {
                fileMsg += ` (${this.getIDEALink(filepath)})`;
            }

            if (lastPath !== filePath) {
                lastPath = filePath;
                dirLogger.info(`${filePath}`);
            }
            logger(color(`${fileStatus}${fileMsg}`));

            if (globals && globalsList) {
                globalsList.filter(g => !g.isExcluded).forEach(({varName, lineInfo, fullFilePath}) => {
                    let lines = colors.cyan(`${lineInfo.line}:${lineInfo.column}`);
                    let globalMsg = `        ${lines} ${varName}`;
                    if (argv['idea-globals']) {
                        globalMsg += ` (${this.getIDEALink(fullFilePath, lineInfo.line, lineInfo.column)})`;
                    }
                    globalLogger.warn(globalMsg);
                });
            }
        });
    };

    this.saveResults = function() {
        const saveLocation = argv['save'];
        if (!saveLocation) {
            return;
        }

        const sortedMessages = _.groupBy(fileMessages,
            message => path.dirname(message.filepath)
        );

        const toSave = {
            files: sortedMessages,
            globals: this._globals,
            stats: {}
        };

        toSave.stats.percentageOfCovered = this._percentageOfConverted();
        toSave.stats.countTotalModules = this._totalModuleCount();

        toSave.stats.countNonAMD = this._countNonAMD;
        toSave.stats.countGlobals = this._countGlobals;
        toSave.stats.countAMDDefine = this._countAMDDefine;
        toSave.stats.countAMDRequire = this._countAMDRequire;

        this._saveToFile(toSave, saveLocation);
    };

    this._saveToFile = function(toSave, saveLocation) {
        fs.writeFile(saveLocation, JSON.stringify(toSave), function(err) {
            if (err) {
                return console.error(err);
            }
        });
    };

    this.printGlobalsSummary = function () {
        if (!isGlobalsSummaryEnabled) {
            return;
        }
        if (argv.files !== false) {
            globalsSummaryLogger.info("=====================================");
        }

        const tmpOutputList = [];

        _(this._globals)
            .filter(gu => !gu.isExcluded)
            .countBy('varName')
            .forEach((count, varName) => tmpOutputList.push({ varName, count }));

        _(tmpOutputList)
            .sortBy(['count', 'varName'])
            .forEach(({count, varName}) => {
                globalsSummaryLogger.info(`${varName} - ${colors.warn(count)}`);
            });
    };

    this._totalModuleCount = function() {
        return this._countAMDDefine + this._countAMDRequire;
    };

    this._percentageOfConverted = function() {
        const totalModuleCount = this._totalModuleCount();
        return totalModuleCount / (totalModuleCount + this._countNonAMD) * 100;
    };

    this.printSummary = function (err) {
        if (err) throw err;

        if (argv.summary === false) {
            return;
        }

        if (argv.files !== false || isGlobalsSummaryEnabled) {
            summaryLogger.info("=====================================");
        }

        const percentage = this._percentageOfConverted();

        summaryLogger.info(`Files identified as AMD modules: ${this._totalModuleCount()}`);
        summaryLogger.info(`  define() calls: ${this._countAMDDefine}`);
        summaryLogger.info(`  require() calls: ${this._countAMDRequire}`);
        summaryLogger.warn(colors.error(`Files not converted: ${this._countNonAMD}`));
        summaryLogger.warn(colors.warn(`Illegal globals found: ${this._countGlobals}`));

        summaryLogger.info(`${isNaN(percentage) ? 0 : Math.round(percentage, 2)}% converted`);
    };

    this.getGlobs = function(searchDir) {
        const globs = [];

        const prefix = `${searchDir}/`;

        includePatterns.forEach(value => {
            globs.push(prefix + value);
        });

        if(config.exclude.patterns) {
            config.exclude.patterns.forEach(value => {
                globs.push(`!${prefix}${value}`);
            });
        }

        return globs;
    };

    this.run = function() {
        this._excludedGlobals = this.setupExcludedGlobals(config.env, Object.keys(globals.builtin));

        const matches = glob.sync(this.getGlobs(searchDir));

        matches.forEach(file => {
            const data = fs.readFileSync(file, 'utf8');

            this.handleFile(null, data, file, () => {});
        });

        this.printFileSummary();
        this.printGlobalsSummary();
        this.printSummary();
        this.saveResults();
    };
}

module.exports = ConversionDetector;