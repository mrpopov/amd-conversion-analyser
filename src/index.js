'use strict';
const argv = require('minimist')(process.argv.slice(2));
const path = require('path');
const fs = require('fs');
const ConversionDetector = require('./lib/conversiondetector');

const FILE_LOG_LEVELS = ['debug', 'info', 'warn', 'error'];
const DEFAULT_FILE_LOG_LEVEL = 'info';

function printHelp() {
    console.log("Run:");
    console.log("   node index.js /your/source/dir <options>");
    console.log("Options:");
    console.log("   --globals         // Show information about used globals per file");
    console.log("   --globals-summary // List all globals used by most to least frequent usage");
    console.log("   --no-utf8         // Don't do pretty print");
    console.log("   --no-summary      // Don't show the conversion summary");
    console.log("   --no-files        // Don't list the files");
    console.log("   --no-ok           // Don't show already converted files");
    console.log("   --save <file>     // Save results to file");
    console.log("   --config <file> // Use this config file");
    console.log("   --idea            // Show a link to open the file directly in IntelliJ IDEA");
    console.log("   --idea-globals    // Show a link to open the file directly in IntelliJ IDEA at the position of the found global");
    console.log(`   --file-log-level  // Set log level for amd conversion status (${FILE_LOG_LEVELS.join(", ")}); default: ${DEFAULT_FILE_LOG_LEVEL}`);
}

function main() {
    if (argv.help) {
        printHelp();
        return;
    }

    if (typeof argv._[0] === 'undefined') {
        console.log("No input directory given. Run with --help for options");
        console.log();
        printHelp();
        return;
    }

    const configPath = [
        argv.config,
        path.join(argv._[0],".amdanalyserrc"),
        path.join(__dirname,"../","config.js")
    ].find(file => {
        try {
            if (!file) return false;
            //accessSync has a weird API. Throws if any accessibility checks fail, and does nothing otherwise.
            fs.accessSync(file, fs.R_OK);
            return fs.statSync(file).isFile();
        } catch(e) {
            return false;
        }
    });

    try {
        console.log("Loading config from " , path.resolve(configPath));
        var config = require(path.resolve(configPath));
    } catch (e) {
        console.error(`Could not load config file: ${e.message}`);
        return;
    }

    config.symbols = require('./lib/symbols')(argv['utf8'] !== false);

    argv['file-log-level'] = FILE_LOG_LEVELS.includes(argv['file-log-level']) ? argv['file-log-level'] : DEFAULT_FILE_LOG_LEVEL;

    const detect = new ConversionDetector(argv._[0], config, argv, console);
    detect.run();
}

main();